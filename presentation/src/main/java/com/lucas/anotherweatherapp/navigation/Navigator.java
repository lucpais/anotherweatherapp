package com.lucas.anotherweatherapp.navigation;

import android.app.Activity;

import com.lucas.anotherweatherapp.features.main.view.MainActivity;

public class Navigator {

    public void navigateToMainActivity(Activity activity) {
        activity.finishAffinity();
        activity.startActivity(MainActivity.getCallingIntent(activity));
    }

}
