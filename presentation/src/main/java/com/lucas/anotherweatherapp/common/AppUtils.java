package com.lucas.anotherweatherapp.common;

import com.lucas.anotherweatherapp.data.repository.network.util.Constants;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AppUtils {

    public static String formatDate(Long epochDate) {
        return new SimpleDateFormat("dd/MM").format(new Date(epochDate * 1000));
    }

    public static String formatTemp(String temp){
        return temp + "°";
    }

    public static String getIconUrl(String openWeatherIcon){
        return Constants.ICON_BASE_URL + openWeatherIcon + "@2x.png";
    }

    public static Double getCelsiusTemp(Double temp){
        return temp - 273;
    }
}
