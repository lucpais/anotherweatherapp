package com.lucas.anotherweatherapp.model.mapper;

import com.lucas.anotherweatherapp.domain.model.WeatherForecastModel;
import com.lucas.anotherweatherapp.model.ForecastDetailModel;

public class ForecastDetailMapper {
    public static ForecastDetailModel transform(WeatherForecastModel.ForecastDayModel model, String city) {
        return (model != null ? new ForecastDetailModel(city, model.getDate(), model.getDayTemp().getTempMin()
                , model.getDayTemp().getTempMax(), model.getWeatherModel().getWeatherMain(),
                model.getWeatherModel().getWeatherDescription(), model.getWeatherModel().getIcon()) : null);
    }
}
