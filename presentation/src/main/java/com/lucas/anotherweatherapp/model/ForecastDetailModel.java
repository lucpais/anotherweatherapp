package com.lucas.anotherweatherapp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class ForecastDetailModel implements Parcelable {

    private String city;
    private Long date;
    private Double tempMin;
    private Double tempMax;
    private String weatherMain;
    private String weatherDescription;
    private String icon;

    public ForecastDetailModel() {
    }

    public ForecastDetailModel(String city, Long date, Double tempMin, Double tempMax, String weatherMain, String weatherDescription, String icon) {
        this.city = city;
        this.date = date;
        this.tempMin = tempMin;
        this.tempMax = tempMax;
        this.weatherMain = weatherMain;
        this.weatherDescription = weatherDescription;
        this.icon = icon;
    }

    public String getCity() {
        return city;
    }

    public Long getDate() {
        return date;
    }

    public Double getTempMin() {
        return tempMin;
    }

    public Double getTempMax() {
        return tempMax;
    }

    public String getWeatherMain() {
        return weatherMain;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public String getIcon() {
        return icon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.city);
        dest.writeValue(this.date);
        dest.writeValue(this.tempMin);
        dest.writeValue(this.tempMax);
        dest.writeString(this.weatherMain);
        dest.writeString(this.weatherDescription);
        dest.writeString(this.icon);
    }

    protected ForecastDetailModel(Parcel in) {
        this.city = in.readString();
        this.date = (Long) in.readValue(Long.class.getClassLoader());
        this.tempMin = (Double) in.readValue(Double.class.getClassLoader());
        this.tempMax = (Double) in.readValue(Double.class.getClassLoader());
        this.weatherMain = in.readString();
        this.weatherDescription = in.readString();
        this.icon = in.readString();
    }

    public static final Parcelable.Creator<ForecastDetailModel> CREATOR = new Parcelable.Creator<ForecastDetailModel>() {
        @Override
        public ForecastDetailModel createFromParcel(Parcel source) {
            return new ForecastDetailModel(source);
        }

        @Override
        public ForecastDetailModel[] newArray(int size) {
            return new ForecastDetailModel[size];
        }
    };
}
