package com.lucas.anotherweatherapp.features.main.di;

import com.lucas.anotherweatherapp.features.main.view.ForecastDetailFragment;
import com.lucas.anotherweatherapp.features.main.view.ForecastListFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBindingModule {

    @ContributesAndroidInjector
    abstract ForecastListFragment provideForecastListFragment();

    @ContributesAndroidInjector
    abstract ForecastDetailFragment provideForecastDetailFragment();

}
