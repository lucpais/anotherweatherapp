package com.lucas.anotherweatherapp.features;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.lucas.anotherweatherapp.internal.di.component.ViewModelFactory;
import com.lucas.anotherweatherapp.navigation.Navigator;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

abstract public class BaseFragment<T extends ViewModel> extends DaggerFragment {
    @Inject
    protected ViewModelFactory viewModelFactory;

    @Inject
    protected Navigator navigator;

    @LayoutRes
    protected abstract int layoutRes();

    protected abstract Class<T> viewModelClazz();

    public T viewModel;
    private Unbinder unbinder;
    private BaseActivity activity;
    protected View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(layoutRes(), container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (viewModelClazz() != null) {
            viewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModelClazz());
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        activity = (BaseActivity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activity = null;
    }


    protected BaseActivity getBaseActivity() {
        return activity;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (unbinder != null) {
            unbinder.unbind();
            unbinder = null;
        }
    }
}
