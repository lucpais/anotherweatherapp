package com.lucas.anotherweatherapp.features;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;

import com.lucas.anotherweatherapp.R;
import com.lucas.anotherweatherapp.internal.di.component.ViewModelFactory;
import com.lucas.anotherweatherapp.navigation.Navigator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;
import io.reactivex.disposables.CompositeDisposable;

abstract public class BaseActivity<T extends ViewModel> extends DaggerAppCompatActivity {

    @Inject
    protected ViewModelFactory viewModelFactory;

    @Inject
    protected Navigator navigator;

    public T viewModel;
    private CompositeDisposable mDisposable;
    boolean backPressedEnable = true;

    @Nullable
    @BindView(R.id.v_progress)
    protected View vProgress;

    @LayoutRes
    protected abstract int layoutRes();

    protected abstract Class<T> viewModelClazz();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutRes());
        ButterKnife.bind(this);
        if (viewModelClazz() != null) {
            viewModel = ViewModelProviders.of(this, viewModelFactory).get(viewModelClazz());
        }
    }

    public void changeLoading(Boolean aBoolean) {
        if (vProgress != null) {
            backPressedEnable = !aBoolean;
            vProgress.setVisibility(aBoolean ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (backPressedEnable) {
            super.onBackPressed();
        }
    }
}
