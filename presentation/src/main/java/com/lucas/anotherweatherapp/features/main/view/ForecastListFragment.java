package com.lucas.anotherweatherapp.features.main.view;

import android.os.Bundle;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lucas.anotherweatherapp.R;
import com.lucas.anotherweatherapp.domain.model.WeatherForecastModel;
import com.lucas.anotherweatherapp.features.BaseFragment;
import com.lucas.anotherweatherapp.features.main.adapter.ForecastWeatherAdapter;
import com.lucas.anotherweatherapp.model.ForecastDetailModel;
import com.lucas.anotherweatherapp.model.mapper.ForecastDetailMapper;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class ForecastListFragment extends BaseFragment<MainViewModel> implements ForecastWeatherAdapter.OnItemClickListener {
    @BindView(R.id.rv_forecast)
    RecyclerView forecastRv;

    @BindView(R.id.searchEt)
    EditText etSearch;

    @Inject
    ForecastWeatherAdapter mAdapter;

    private WeatherForecastModel forecastModel;

    @Override
    protected int layoutRes() {
        return R.layout.fragment_forecast_list;
    }

    @Override
    protected Class<MainViewModel> viewModelClazz() {
        return MainViewModel.class;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setUpRecycler();
        observeViewModel();
    }

    private void observeViewModel() {
        viewModel.getLoadingData().observe(getViewLifecycleOwner(), new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                getBaseActivity().changeLoading(aBoolean);
            }
        });
        viewModel.getForecastInfo().observe(getViewLifecycleOwner(), new Observer<WeatherForecastModel>() {
            @Override
            public void onChanged(WeatherForecastModel model) {
                forecastModel = model;
                if (forecastModel != null) {
                    mAdapter.setData(forecastModel.getForecastDayList());
                }
            }
        });
    }

    private void setUpRecycler() {
        LinearLayoutManager lm = new LinearLayoutManager(getBaseActivity());
        forecastRv.setLayoutManager(lm);
        DividerItemDecoration itemDecor = new DividerItemDecoration(getBaseActivity(), lm.getOrientation());
        forecastRv.addItemDecoration(itemDecor);
        mAdapter.setListener(this);
        forecastRv.setAdapter(mAdapter);
    }

    @OnClick(R.id.searchBtn)
    public void onClickSearch() {
        if (etSearch != null && !etSearch.getText().toString().isEmpty())
            viewModel.retrieveForecast(etSearch.getText().toString());
    }

    @Override
    public void itemClick(int index) {
        ForecastDetailModel modelArg = ForecastDetailMapper.transform(forecastModel.getForecastDayList().get(index), forecastModel.getCity().getName());
        ForecastListFragmentDirections.ActionForecastListFragmentToForecastDetailFragment action =
                ForecastListFragmentDirections.actionForecastListFragmentToForecastDetailFragment();
        action.setForecastDetailModel(modelArg);
        Navigation.findNavController(getView()).navigate(action);
    }
}
