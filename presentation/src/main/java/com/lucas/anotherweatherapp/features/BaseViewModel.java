package com.lucas.anotherweatherapp.features;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Build;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.lucas.anotherweatherapp.domain.usecase.BaseUseCase;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

abstract public class BaseViewModel extends ViewModel {

    @Inject
    protected Context context;

    private Disposable disposable = Disposables.empty();

    final private MutableLiveData<Boolean> loadingData = new MutableLiveData<>();
    final private MutableLiveData<Boolean> connectionData = new MutableLiveData<>();
    final private MutableLiveData<String> stringData = new MutableLiveData<>();
    final private MutableLiveData<String> loginDeepLinkData = new MutableLiveData<>();
    final private MutableLiveData<Throwable> errorData = new MutableLiveData<>();

    @SuppressWarnings("unchecked")
    public <T> void execute(BaseUseCase<T> useCase, BaseObserver useCaseSubscriber) {
        this.disposable = Observable.create(emitter -> {
            try {
                emitter.onNext(useCase.execute());
                emitter.onComplete();
            } catch (Exception e) {
                useCaseSubscriber.onError(e);
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(useCaseSubscriber);
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Network[] networks = connectivityManager.getAllNetworks();
            NetworkInfo networkInfo;
            for (Network mNetwork : networks) {
                networkInfo = connectivityManager.getNetworkInfo(mNetwork);
                if (networkInfo.getState().equals(NetworkInfo.State.CONNECTED)) {
                    return true;
                }
            }
        } else {
            if (connectivityManager != null) {
                NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
                if (info != null) {
                    for (NetworkInfo anInfo : info) {
                        if (anInfo.getState() == NetworkInfo.State.CONNECTED) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }


    protected abstract class BaseObserver<T> extends DisposableObserver<T> implements Observer<T> {

        @Override
        protected void onStart() {
            super.onStart();
            loadingData.postValue(true);
        }

        @Override
        public void onNext(T t) {
        }

        @Override
        public void onError(Throwable t) {
            errorData.postValue(t);
            loadingData.postValue(false);
        }

        @Override
        public void onComplete() {
            loadingData.postValue(false);
        }

    }


    public Disposable getDisposable() {
        return disposable;
    }

    protected void disposeEvent() {
        if (getDisposable() != null && !getDisposable().isDisposed()) {
            getDisposable().dispose();
        }
    }

    public MutableLiveData<Boolean> getLoadingData() {
        return loadingData;
    }

    public MutableLiveData<Throwable> getErrorData() {
        return errorData;
    }

    public MutableLiveData<String> getStringData() {
        return stringData;
    }

    public MutableLiveData<String> getLoginDeepLinkData() {
        return loginDeepLinkData;
    }

    public MutableLiveData<Boolean> getConnectionData() {
        return connectionData;
    }

}
