package com.lucas.anotherweatherapp.features.main.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lucas.anotherweatherapp.R;
import com.lucas.anotherweatherapp.common.AppUtils;
import com.lucas.anotherweatherapp.domain.model.WeatherForecastModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForecastWeatherAdapter extends RecyclerView.Adapter<ForecastWeatherAdapter.MyViewHolder> {
    private final Context mContext;
    private List<WeatherForecastModel.ForecastDayModel> mData;
    private OnItemClickListener listener;

    @Inject
    public ForecastWeatherAdapter(Context context) {
        mContext = context;
    }

    public void setListener(OnItemClickListener onItemClickListener) {
        this.listener = onItemClickListener;
    }

    public void setData(List<WeatherForecastModel.ForecastDayModel> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.forecast_max_temp)
        public TextView mMaxTemp;

        @BindView(R.id.forecast_min_temp)
        public TextView mMinTemp;

        @BindView(R.id.forecast_date)
        public TextView mDate;

        @BindView(R.id.forecast_image)
        public ImageView mWeatherThumbnail;

        @OnClick()
        void itemClicked() {
            listener.itemClick(getAdapterPosition());
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.forecast_item, viewGroup, false);
        ForecastWeatherAdapter.MyViewHolder vh = new ForecastWeatherAdapter.MyViewHolder(itemView);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        myViewHolder.mMaxTemp.setText(AppUtils.formatTemp(mData.get(i).getDayTemp().getTempMax().toString()));
        myViewHolder.mMinTemp.setText(AppUtils.formatTemp(mData.get(i).getDayTemp().getTempMin().toString()));
        myViewHolder.mDate.setText(AppUtils.formatDate(mData.get(i).getDate()));
        Picasso.get()
                .load(AppUtils.getIconUrl(mData.get(i).getWeatherModel().getIcon()))
                .resize((int) mContext.getResources().getDimension(R.dimen.width_image_thumb), (int) mContext.getResources().getDimension(R.dimen.height_image_thumb))
                .error(android.R.drawable.ic_menu_camera)
                .placeholder(android.R.drawable.ic_menu_camera)
                .into(myViewHolder.mWeatherThumbnail);
    }

    @Override
    public int getItemCount() {
        if (mData != null) {
            return mData.size();
        } else {
            return 0;
        }
    }

    public interface OnItemClickListener {
        void itemClick(int index);
    }

}
