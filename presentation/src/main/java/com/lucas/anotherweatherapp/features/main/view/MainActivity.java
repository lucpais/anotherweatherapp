package com.lucas.anotherweatherapp.features.main.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;

import com.lucas.anotherweatherapp.R;
import com.lucas.anotherweatherapp.domain.model.WeatherForecastModel;
import com.lucas.anotherweatherapp.features.BaseActivity;

import butterknife.BindView;

public class MainActivity extends BaseActivity<MainViewModel> {

    private WeatherForecastModel forecastModel;

    public void setForecastModel(WeatherForecastModel forecastModel) {
        this.forecastModel = forecastModel;
    }

    public WeatherForecastModel getForecastModel() {
        return forecastModel;
    }

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected Class<MainViewModel> viewModelClazz() {
        return MainViewModel.class;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


}
