package com.lucas.anotherweatherapp.features.main.view;

import androidx.lifecycle.MutableLiveData;

import com.lucas.anotherweatherapp.domain.model.WeatherForecastModel;
import com.lucas.anotherweatherapp.domain.usecase.app.ForecastUseCase;
import com.lucas.anotherweatherapp.features.BaseViewModel;

import javax.inject.Inject;

public class MainViewModel extends BaseViewModel {

    final ForecastUseCase forecastUseCase;
    final private MutableLiveData<WeatherForecastModel> forecastInfo = new MutableLiveData<>();

    @Inject
    public MainViewModel(ForecastUseCase forecastUseCase) {
        this.forecastUseCase = forecastUseCase;
    }

    public void retrieveForecast(String searchCreiteria) {
        forecastUseCase.bind(searchCreiteria);
        execute(forecastUseCase, new RetrieveForecastObservable());
    }

    public MutableLiveData<WeatherForecastModel> getForecastInfo() {
        return forecastInfo;
    }

    private class RetrieveForecastObservable extends BaseObserver<WeatherForecastModel>{

        @Override
        public void onNext(WeatherForecastModel s) {
            super.onNext(s);
            forecastInfo.postValue(s);
        }

    }
}
