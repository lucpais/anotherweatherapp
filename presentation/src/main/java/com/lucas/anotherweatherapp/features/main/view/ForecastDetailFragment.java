package com.lucas.anotherweatherapp.features.main.view;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.lucas.anotherweatherapp.R;
import com.lucas.anotherweatherapp.common.AppUtils;
import com.lucas.anotherweatherapp.features.BaseFragment;
import com.lucas.anotherweatherapp.model.ForecastDetailModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class ForecastDetailFragment extends BaseFragment<MainViewModel> {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.forecast_image)
    ImageView forecastImage;

    @BindView(R.id.forecast_date)
    TextView tvForecastDate;
    @BindView(R.id.forecast_max_temp)
    TextView tvMaxTemp;
    @BindView(R.id.forecast_min_temp)
    TextView tvMinTemp;
    @BindView(R.id.tv_weather_main)
    TextView tvWeatherMain;
    @BindView(R.id.tv_weather_description)
    TextView tvWeatherDescription;

    private ForecastDetailModel detailModel;

    @Override
    protected int layoutRes() {
        return R.layout.fragment_forecast_day_detail;
    }

    @Override
    protected Class<MainViewModel> viewModelClazz() {
        return MainViewModel.class;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews();
    }

    private void initViews() {
        toolbar.setNavigationOnClickListener(view -> getBaseActivity().onBackPressed());
        if (getArguments() != null) {
            detailModel = ForecastDetailFragmentArgs.fromBundle(getArguments()).getForecastDetailModel();
            Picasso.get()
                    .load(AppUtils.getIconUrl(detailModel.getIcon()))
                    .resize((int) getBaseActivity().getResources().getDimension(R.dimen.width_image_thumb), (int) getBaseActivity().getResources().getDimension(R.dimen.height_image_thumb))
                    .error(android.R.drawable.ic_menu_camera)
                    .placeholder(android.R.drawable.ic_menu_camera)
                    .into(forecastImage);
            toolbar.setTitle(detailModel.getCity());
            tvForecastDate.setText(AppUtils.formatDate(detailModel.getDate()));
            tvMaxTemp.setText(AppUtils.formatTemp(detailModel.getTempMax().toString()));
            tvMinTemp.setText(AppUtils.formatTemp(detailModel.getTempMin().toString()));
            tvWeatherMain.setText(detailModel.getWeatherMain());
            tvWeatherDescription.setText(detailModel.getWeatherDescription());
        }
    }

}
