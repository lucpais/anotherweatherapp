package com.lucas.anotherweatherapp.internal.di.module;

import com.lucas.anotherweatherapp.BuildConfig;
import com.lucas.anotherweatherapp.data.repository.network.WeatherApi;
import com.lucas.anotherweatherapp.data.repository.network.api.AppNetwork;
import com.lucas.anotherweatherapp.data.repository.network.util.Constants;
import com.lucas.anotherweatherapp.domain.repository.network.AppNetworkRepository;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    @Singleton
    @Named("Api")
    WeatherApi provideApi(@Named("RetrofitApi") Retrofit retrofit) {
        return retrofit.create(WeatherApi.class);
    }

    @Provides
    @Singleton
    @Named("RetrofitApi")
    Retrofit provideRetrofit(GsonConverterFactory gsonConverterFactory, @Named("OkHttpClient") OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(gsonConverterFactory)
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    @Named("OkHttpClient")
    OkHttpClient provideOkHttpClientTransactional(HttpLoggingInterceptor httpLoggingInterceptor) {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient().newBuilder();
        httpClientBuilder.connectTimeout(30, TimeUnit.SECONDS);
        httpClientBuilder.readTimeout(30, TimeUnit.SECONDS);
        httpClientBuilder.writeTimeout(30, TimeUnit.SECONDS);
        httpClientBuilder.addInterceptor(chain -> {
            Request.Builder builder = chain.request().newBuilder();
            return chain.proceed(builder.build());
        });
        httpClientBuilder.addInterceptor(httpLoggingInterceptor);
        httpClientBuilder.addInterceptor(new WeatherAppInterceptor());
        return httpClientBuilder.build();
    }
    @Provides
    @Singleton
    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return logging;
    }

    public class WeatherAppInterceptor implements Interceptor {
        @NotNull
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Response response = chain.proceed(request);
            return response;
        }
    }

    @Provides
    @Singleton
    AppNetworkRepository provideAppNetworkRepository(AppNetwork appNetwork) {
        return appNetwork;
    }

}
