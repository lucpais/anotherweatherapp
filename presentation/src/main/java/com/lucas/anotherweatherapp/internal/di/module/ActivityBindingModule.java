package com.lucas.anotherweatherapp.internal.di.module;

import com.lucas.anotherweatherapp.features.main.di.MainFragmentBindingModule;
import com.lucas.anotherweatherapp.features.main.view.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract MainActivity bindMainActivity();

}