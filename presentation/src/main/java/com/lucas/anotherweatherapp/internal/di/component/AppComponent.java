package com.lucas.anotherweatherapp.internal.di.component;

import android.app.Application;

import com.lucas.anotherweatherapp.AnotherWeatherApplication;
import com.lucas.anotherweatherapp.internal.di.module.ActivityBindingModule;
import com.lucas.anotherweatherapp.internal.di.module.ContextModule;
import com.lucas.anotherweatherapp.internal.di.module.NavigatorModule;
import com.lucas.anotherweatherapp.internal.di.module.NetworkModule;
import com.lucas.anotherweatherapp.internal.di.module.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.support.AndroidSupportInjectionModule;


/*
 * We mark this interface with the @Component annotation.
 * And we define all the modules that can be injected.
 * Note that we provide AndroidSupportInjectionModule.class
 * here. This class was not created by us.
 * It is an internal class in Dagger 2.10.
 * Provides our activities and fragments with given module.
 * */
@Component(modules = {
        NetworkModule.class,
        ContextModule.class,
        ActivityBindingModule.class,
        NavigatorModule.class,
        AndroidSupportInjectionModule.class,
        ViewModelModule.class})

@Singleton
public interface AppComponent extends AndroidInjector<DaggerApplication> {

    /* We will call this builder interface from our custom Application class.
     * This will set our application object to the AppComponent.
     * So inside the AppComponent the application instance is available.
     * So this application instance can be accessed by our modules
     * such as ApiModule when needed
     * */
    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    /*
     * This is our custom Application class
     * */
    void inject(AnotherWeatherApplication application);

}

