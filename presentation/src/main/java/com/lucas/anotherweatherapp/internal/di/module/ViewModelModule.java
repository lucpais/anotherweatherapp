package com.lucas.anotherweatherapp.internal.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.lucas.anotherweatherapp.features.main.view.MainViewModel;
import com.lucas.anotherweatherapp.internal.di.component.ViewModelFactory;
import com.lucas.anotherweatherapp.internal.di.component.ViewModelKey;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel mainViewModel);


    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
