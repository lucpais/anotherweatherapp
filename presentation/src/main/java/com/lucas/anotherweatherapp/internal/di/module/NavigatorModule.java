package com.lucas.anotherweatherapp.internal.di.module;

import com.lucas.anotherweatherapp.navigation.Navigator;

import dagger.Module;
import dagger.Provides;

@Module
public class NavigatorModule {

    @Provides
    Navigator provideNavigator() {
        return new Navigator();
    }
}
