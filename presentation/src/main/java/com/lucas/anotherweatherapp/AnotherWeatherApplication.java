package com.lucas.anotherweatherapp;

import android.app.Activity;

import com.lucas.anotherweatherapp.internal.di.component.AppComponent;
import com.lucas.anotherweatherapp.internal.di.component.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.DispatchingAndroidInjector;

public class AnotherWeatherApplication extends DaggerApplication {

    private static AnotherWeatherApplication instance;

    public AnotherWeatherApplication() {
        instance = this;
    }

    public static AnotherWeatherApplication getInstance() {
        return instance;
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent component = DaggerAppComponent.builder().application(this).build();
        component.inject(this);
        return component;
    }

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

}
