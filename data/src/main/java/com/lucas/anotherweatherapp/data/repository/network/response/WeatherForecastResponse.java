package com.lucas.anotherweatherapp.data.repository.network.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class WeatherForecastResponse {
    @SerializedName("city")
    private City city;

    @SerializedName("list")
    private List<ForecastDay> forecastDayList;

    public City getCity() {
        return city;
    }

    public List<ForecastDay> getForecastDayList() {
        return forecastDayList;
    }

    public static class City {
        @SerializedName("name")
        private String name;

        public String getName() {
            return name;
        }
    }

    public static class ForecastDay {
        @SerializedName("dt")
        private Long date;

        @SerializedName("temp")
        private Temp dayTemp;

        @SerializedName("weather")
        private List<Weather> forecastDayWeatherList;

        public Long getDate() {
            return date;
        }

        public Temp getDayTemp() {
            return dayTemp;
        }

        public List<Weather> getForecastDayWeatherList() {
            return forecastDayWeatherList;
        }
    }

    public static class Temp {
        @SerializedName("min")
        private Double tempMin;
        @SerializedName("max")
        private Double tempMax;

        public Double getTempMin() {
            return tempMin;
        }

        public Double getTempMax() {
            return tempMax;
        }
    }

    public static class Weather {
        @SerializedName("main")
        private String weatherMain;

        @SerializedName("description")
        private String weatherDescription;

        @SerializedName("icon")
        private String icon;

        public String getWeatherMain() {
            return weatherMain;
        }

        public String getWeatherDescription() {
            return weatherDescription;
        }

        public String getIcon() {
            return icon;
        }
    }
}
