package com.lucas.anotherweatherapp.data.repository.network.api;

import com.lucas.anotherweatherapp.data.repository.network.WeatherApi;
import com.lucas.anotherweatherapp.data.repository.network.response.WeatherForecastResponse;
import com.lucas.anotherweatherapp.data.repository.network.response.mapper.ForecastResponseMapper;
import com.lucas.anotherweatherapp.domain.model.WeatherForecastModel;
import com.lucas.anotherweatherapp.domain.repository.network.AppNetworkRepository;

import javax.inject.Inject;
import javax.inject.Named;

import retrofit2.Call;

import static com.lucas.anotherweatherapp.data.repository.network.util.Constants.API_KEY;
import static com.lucas.anotherweatherapp.data.repository.network.util.Constants.FORECAST_COUNT;
import static com.lucas.anotherweatherapp.data.repository.network.util.Constants.FORECAST_UNIT;

public class AppNetwork implements AppNetworkRepository {

    private WeatherApi weatherApi;

    @Inject
    public AppNetwork(@Named("Api") WeatherApi weatherApi) {
        this.weatherApi = weatherApi;
    }

    @Override
    public WeatherForecastModel getForecast(String cityName) throws Exception {

        Call<WeatherForecastResponse> call = weatherApi.getWeatherForecast(cityName, FORECAST_UNIT, FORECAST_COUNT, API_KEY);
        WeatherForecastResponse response = call.execute().body();
        return ForecastResponseMapper.transform(response);
    }
}
