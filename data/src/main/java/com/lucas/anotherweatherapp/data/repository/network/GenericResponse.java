package com.lucas.anotherweatherapp.data.repository.network;

import java.util.Objects;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Response;

public class GenericResponse<T> {

    private T response;
    private Exception exception;

    public GenericResponse(Response<T> responseNetwork) {
        switch (responseNetwork.code()) {
            case HttpsURLConnection.HTTP_OK:
            case HttpsURLConnection.HTTP_CREATED:
            case HttpsURLConnection.HTTP_ACCEPTED:
            case HttpsURLConnection.HTTP_NO_CONTENT:
                response = responseNetwork.body();
                break;
            default:
                exception = new Exception(Objects.requireNonNull(responseNetwork.errorBody()).toString());
                break;

        }
    }

    public T getResponse() {
        return response;
    }

    public Exception getException() {
        return exception;
    }

}
