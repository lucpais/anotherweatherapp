package com.lucas.anotherweatherapp.data.repository.network.response.mapper;

import com.lucas.anotherweatherapp.data.repository.network.response.WeatherForecastResponse;
import com.lucas.anotherweatherapp.domain.model.WeatherForecastModel;

import java.util.ArrayList;
import java.util.List;

public class ForecastResponseMapper {

    public static WeatherForecastModel transform(WeatherForecastResponse response) {

        return (response != null) ? new WeatherForecastModel(new WeatherForecastModel.CityModel(response.getCity().getName()),
                transform(response.getForecastDayList())) : null;
    }

    public static List<WeatherForecastModel.ForecastDayModel> transform(List<WeatherForecastResponse.ForecastDay> forecastDaysList) {

        List<WeatherForecastModel.ForecastDayModel> list = new ArrayList<>();
        if (forecastDaysList != null) {
            for (WeatherForecastResponse.ForecastDay response : forecastDaysList) {
                list.add(transform(response));
            }
        }
        return list;
    }

    public static WeatherForecastModel.ForecastDayModel transform(WeatherForecastResponse.ForecastDay forecastedDay) {
        return (forecastedDay != null) ? new WeatherForecastModel.ForecastDayModel(forecastedDay.getDate(),
                new WeatherForecastModel.TempModel(forecastedDay.getDayTemp().getTempMin(),forecastedDay.getDayTemp().getTempMax()),
                new WeatherForecastModel.WeatherModel(forecastedDay.getForecastDayWeatherList().get(0).getWeatherMain(),
                        forecastedDay.getForecastDayWeatherList().get(0).getWeatherDescription(),
                        forecastedDay.getForecastDayWeatherList().get(0).getIcon())) : null;
    }


}
