package com.lucas.anotherweatherapp.data.repository.network;

import com.lucas.anotherweatherapp.data.repository.network.response.WeatherForecastResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherApi {

    @GET("forecast/daily?")
    Call<WeatherForecastResponse> getWeatherForecast(@Query("q") String city, @Query("units") String units, @Query("cnt") String count, @Query("appid") String app_id);
}

