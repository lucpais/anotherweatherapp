package com.lucas.anotherweatherapp.data.repository.network.util;

public class Constants {
    public static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";
    public static final String ICON_BASE_URL = "https://openweathermap.org/img/wn/";
    public static final String API_KEY = "886705b4c1182eb1c69f28eb8c520e20";
    public static final String FORECAST_COUNT = "16";
    public static final String FORECAST_UNIT = "metric";
}
