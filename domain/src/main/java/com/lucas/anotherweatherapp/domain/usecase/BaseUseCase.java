package com.lucas.anotherweatherapp.domain.usecase;

abstract public class BaseUseCase<T> {
    public abstract T execute() throws  Exception;
}
