package com.lucas.anotherweatherapp.domain.repository.network;

import com.lucas.anotherweatherapp.domain.model.WeatherForecastModel;

public interface AppNetworkRepository {

    WeatherForecastModel getForecast(String cityName) throws Exception;
}
