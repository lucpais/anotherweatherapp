package com.lucas.anotherweatherapp.domain.usecase.app;

import com.lucas.anotherweatherapp.domain.model.WeatherForecastModel;
import com.lucas.anotherweatherapp.domain.repository.network.AppNetworkRepository;
import com.lucas.anotherweatherapp.domain.usecase.BaseUseCase;

import javax.inject.Inject;

public class ForecastUseCase extends BaseUseCase<WeatherForecastModel> {

    private final AppNetworkRepository appNetworkRepository;
    private String city;

    @Inject
    public ForecastUseCase(AppNetworkRepository appNetworkRepository) {
        this.appNetworkRepository = appNetworkRepository;
    }

    public void bind(String city) {
        this.city = city;
    }

    @Override
    public WeatherForecastModel execute() throws Exception {
        return appNetworkRepository.getForecast(city);
    }
}
