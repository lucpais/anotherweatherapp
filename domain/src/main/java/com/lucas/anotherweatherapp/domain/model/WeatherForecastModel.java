package com.lucas.anotherweatherapp.domain.model;

import java.io.Serializable;
import java.util.List;

public class WeatherForecastModel implements Serializable {

    public WeatherForecastModel(CityModel city, List<ForecastDayModel> forecastDayList) {
        this.city = city;
        this.forecastDayList = forecastDayList;
    }

    private CityModel city;

    private List<ForecastDayModel> forecastDayList;

    public CityModel getCity() {
        return city;
    }

    public List<ForecastDayModel> getForecastDayList() {
        return forecastDayList;
    }

    public void setCity(CityModel city) {
        this.city = city;
    }

    public void setForecastDayList(List<ForecastDayModel> forecastDayList) {
        this.forecastDayList = forecastDayList;
    }

    public static class CityModel implements Serializable {
        private String name;

        public String getName() {
            return name;
        }

        public CityModel(String name) {
            this.name = name;
        }
    }

    public static class ForecastDayModel implements Serializable {
        private Long date;

        private TempModel dayTemp;

        private WeatherModel weatherModel;

        public ForecastDayModel(Long date, TempModel dayTemp, WeatherModel forecastDayWeatherList) {
            this.date = date;
            this.dayTemp = dayTemp;
            this.weatherModel = forecastDayWeatherList;
        }

        public Long getDate() {
            return date;
        }

        public TempModel getDayTemp() {
            return dayTemp;
        }

        public void setDate(Long date) {
            this.date = date;
        }

        public void setDayTemp(TempModel dayTemp) {
            this.dayTemp = dayTemp;
        }

        public WeatherModel getWeatherModel() {
            return weatherModel;
        }

        public void setWeatherModel(WeatherModel weatherModel) {
            this.weatherModel = weatherModel;
        }
    }

    public static class TempModel implements Serializable {
        private Double tempMin;
        private Double tempMax;

        public Double getTempMin() {
            return tempMin;
        }

        public Double getTempMax() {
            return tempMax;
        }

        public TempModel(Double tempMin, Double tempMax) {
            this.tempMin = tempMin;
            this.tempMax = tempMax;
        }

        public void setTempMin(Double tempMin) {
            this.tempMin = tempMin;
        }

        public void setTempMax(Double tempMax) {
            this.tempMax = tempMax;
        }
    }

    public static class WeatherModel implements Serializable {
        private String weatherMain;

        private String weatherDescription;

        private String icon;

        public String getWeatherMain() {
            return weatherMain;
        }

        public String getWeatherDescription() {
            return weatherDescription;
        }

        public String getIcon() {
            return icon;
        }

        public void setWeatherMain(String weatherMain) {
            this.weatherMain = weatherMain;
        }

        public void setWeatherDescription(String weatherDescription) {
            this.weatherDescription = weatherDescription;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

        public WeatherModel(String weatherMain, String weatherDescription, String icon) {
            this.weatherMain = weatherMain;
            this.weatherDescription = weatherDescription;
            this.icon = icon;
        }
    }
}
